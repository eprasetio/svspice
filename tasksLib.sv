/*=================================================================================
****	iSPICE EE 537: taskLib.sv
****	-- Tasks Libraries --
****
****	by Edwin Prasetio
****
**** Libraries of Tasks that's being used for the main iSPICE program
****
**** Tasks & Functions List:
**** 	- executeAction()
****	- runDCAnalysis()
****	- runTransientAnalysis(integer f)
****	- updateVectorB()
**** 	- stampElements()
****	- defineDiodeParamater(string dmodel)
**** 	- performLUFactorization()
****	- solveAxBWithLUDecomposition()
****	- getElements(string inputStr, ref string elementInALineArray[int])
**** 	- int convertNodeToInt(string node_s)
****	- netlistParser(string file_path)
****	- updateMaxNode()
**** 	- initMatrixAndVectors()
****	- resetMatrixAndVectors()
**** 	- throwErrorMsgAndExit(string error_msg)
**** 	- displayBufferedNetList()
****	- displayElementArray()
**** 	- displayMatrixAndVectors()
****	- displayDCAnalysisReport()
****	- printBufferedNetList(integer f)
****	- printMatrixAndVectorsToAFile(integer f)
****	- printDCAnalysisReportToAFile(integer f)
****
=================================================================================*/
 
/*********************************************************************************
**** Task for executing action
**********************************************************************************/
 task executeAction(); 
	integer outputf;
	
	// open output file for dc analysis report
	outputf = $fopen(output_file_path, "w");
	
	// writing DC Analysis result to a file
	$fwrite(outputf, "\n==================================================================================================\n");
	$fwrite(outputf, "####################################### iSPICE Test Result #######################################\n");
	$fwrite(outputf, "==================================================================================================\n\n");
	
	for(int i=0;i<action.num();i++) begin
		case (action[i])
			DC: begin
				runDCAnalysis(); // perform DC analysis
			
				displayDCAnalysisReport(); // print DC analysis report
				
				printBufferedNetList(outputf); // print netlist
				printMatrixAndVectorsToAFile(outputf); // print matrices and vectors
				printDCAnalysisReportToAFile(outputf); // print DC analysis report to a file
			end
			OP: begin
				runDCAnalysis(); // perform DC analysis
			
				displayMatrixAndVectors(); // print all matrices and vectors
				displayDCAnalysisReport(); // print DC analysis report
				
				printBufferedNetList(outputf); // print netlist
				printMatrixAndVectorsToAFile(outputf); // print matrices and vectors
				printDCAnalysisReportToAFile(outputf); // print DC analysis report to a file
			end
			TRAN: begin
				runDCAnalysis(); // perform DC analysis
			
				printBufferedNetList(outputf); // print netlist
				printMatrixAndVectorsToAFile(outputf); // print matrices and vectors
				printDCAnalysisReportToAFile(outputf); // print DC analysis report to a file
	
				runTransientAnalysis(outputf); // perform transient analysis
			end
			NETLIST: begin
				displayBufferedNetList(); // print buffered netlist - exclude white spaces
			end
			NONE: begin
				// Do nothing
			end
			default: begin
				// Do nothing as default
			end
		endcase
	end
	
	$fwrite(outputf, "\n==================================================================================================\n");
	$fwrite(outputf, "##################################################################################################\n");
	$fwrite(outputf, "==================================================================================================\n\n");
	
	$fclose(outputf);	
 
 endtask

/*********************************************************************************
**** Task for performing DC analysis
**********************************************************************************/
 task runDCAnalysis();
	real diff;
	
	$display("## Starting DC analysis... ##");
	
	// construct the vector xPrev using dense format. 
	// This is for storing prev result
	for(int j=0;j<(matrix_size);j++) begin
		stmp = new();
		stmp.setValue(0.0);
		vector_xPrev[j] = stmp;
	end
	
	// perform newton rapshon iteration to handle non-linear component
	do begin

		//$display("SIZE : %d", matrix_size);
		// reset matrix A & LU 
		for(int i=0;i<(matrix_size*matrix_size);i++) begin
			matrix_lu[i].setValue(0);
			matrix_a[i].setValue(0);
		end
	
		//reset vector B
		for(int k=0;k<(matrix_size);k++) begin
			vector_b[k].setValue(0);
		end
	
		stampElements(); // stamp elements
		
		// reset vector X & Y
		for(int j=0;j<(matrix_size);j++) begin
			vector_x[j].setValue(0);
			vector_y[j].setValue(0);
		end
		
		performLUFactorization(); // generata LU matrix from matrix A
		solveAxBWithLUDecomposition(); // solve Ax=B system
		
		/*$display("Printing Vector X: ");
		for(int j=0;j<(matrix_size);j++) begin
			$write("\t[ ");
			$write("%.6f", vector_x[j].getValue() );
			$write(" ]\n");
		end
		$write("\n");*/
		
		// determine if the value has converged
		for(int j=0;j<(matrix_size - vs_tracker.getNumbOfVS());j++) begin
			diff = vector_x[j].getValue() - vector_xPrev[j].getValue();
			
			if( diff < 0) begin // get absolute value
				diff = -1*diff;
			end
			
			//$display("VX : %f", vector_x[j].getValue());
			//$display("VX PREV : %f",vector_xPrev[j].getValue());
			//$display("VX DIFF : %f\n", diff);
			
			if( diff < 0.00001) begin
				converge = 1; // set to 1 if result converged
			end
			else begin
				converge = 0; // set to 0 if result not converge yet
				break;
			end
		end
		
		// copy result vector x to a temporary array
		for(int j=0;j<(matrix_size);j++) begin
			vector_xPrev[j].setValue(vector_x[j].getValue());
			//$display(vector_xPrev[j].getValue());
		end
		
		matrix_size = matrix_size + 1; // reset matrix size back to original
		
		//$display("Convergence Val: %d", converge);
		
	end while ( !converge );
	
	matrix_size = matrix_size -1; // change matrix size minus gnd's row & column
	
	$display("## DC analysis is done ##");
	
 endtask
 
 
/*********************************************************************************
**** Task for performing transient analysis
**********************************************************************************/
 task runTransientAnalysis(integer f);
	
	$display("## Starting transient analysis... ##");
	
	// construct the vector trans using dense format
	for(int j=0;j<(matrix_size+1);j++) begin
		stmp = new();
		stmp.setValue(0.0);
		vector_trans[j] = stmp;
	end
	
	$fwrite(f, "\n+----------------------- Transient Analysis Report -----------------------+\n\n"); 
	
	// result header
	$write("\tTime Stamp(s)");
	$fwrite(f, "\tTime Stamp(s)");
	for(int k=0;k<(matrix_size-vs_tracker.getNumbOfVS());k++) begin
		$write("%d(V)", k+1);
		$fwrite(f, "%d(V)", k+1);
	end
	$write("\t");
	$fwrite(f, "\t");
	for(int k=(matrix_size-vs_tracker.getNumbOfVS());k<matrix_size;k++) begin
		$write("\t%s(A)\t",  vs_tracker.getNameByIndex(k-(matrix_size-vs_tracker.getNumbOfVS())));
		$fwrite(f, "\t%s(A)\t",  vs_tracker.getNameByIndex(k-(matrix_size-vs_tracker.getNumbOfVS())));
	end
	$write("\n");
	$fwrite(f, "\n");
		
	// run transient analysis iteration
	for(real i=0;i<time_max+time_step;i=i+time_step) begin
		// show result here
		$write("\t%.9f", i);
		$fwrite(f, "\t%.9f", i);
		for(int k=0;k<(matrix_size-vs_tracker.getNumbOfVS());k++) begin
			$write("\t%.6f", vector_x[k].getValue());
			$fwrite(f, "\t%.6f", vector_x[k].getValue());
		end
		for(int k=(matrix_size-vs_tracker.getNumbOfVS());k<matrix_size;k++) begin
			$write("\t%.6f", vector_x[k].getValue());
			$fwrite(f, "\t%.6f", vector_x[k].getValue());
		end
		$write("\n");
		$fwrite(f, "\n");
		
		updateVectorB();
		solveAxBWithLUDecomposition(); 
		
	end
	
	$fwrite(f, "+------------------------- ++++++++++++++++++ -------------------------+\n\n"); 
	$display("## Finished transient analysis ##");
	
 endtask 
 
/*********************************************************************************
**** Task for updating vector b for transient analysis
**********************************************************************************/
 task updateVectorB();
	int m_size;
	int size_min_vs;
	int idx;
	real c_volt, l_volt, c_curr, l_curr;
	int node_p, node_n;
	
	c_volt = 0;
	l_volt = 0;
	c_curr = 0;
	l_curr = 0;
	
	// zeroed vector trans first
	// construct the vector X using dense format
	for(int j=0;j<(matrix_size+1);j++) begin
		vector_trans[j].setValue(0);
	end
	
	//$display("\t# Updating vector b... #");
	m_size = max_node + vs_tracker.getNumbOfVS() + 1; // let matrix size = the highest number of node
	size_min_vs = matrix_size+1 - vs_tracker.getNumbOfVS();
	// restamp vector B
	for(int i=0;i<(elArray.num());i++) begin
		case ( elArray[i].getType() )
			CAPACITOR: begin
				//only do stamping if node > 0. If node < 1, do nothing since its row will be removed anyway
				node_p = elArray[i].getNodeP();
				node_n = elArray[i].getNodeN();
				// if node_p is greater than 1, subtract node p index
				if(node_p > 0 && node_n > 0) begin
					node_p = node_p-1;
					node_n = node_n-1;
					c_volt = vector_x[ node_p ].getValue() - vector_x[ node_n].getValue(); // get previous voltage value
					c_curr = (c_volt*2*elArray[i].getValue()/time_step) - (vector_b[ node_p ].getValue()); // get previous current value
					//$display("C_volt P: %f",  c_volt);
					//$display("C_curr P: %f",  c_curr);
					vector_trans[ elArray[i].getNodeP() ].addValue( 2*elArray[i].getValue()*c_volt/time_step + c_curr ); // stamp for node p
					vector_trans[ elArray[i].getNodeN() ].addValue( -2*elArray[i].getValue()*c_volt/time_step - c_curr ); // stamp for node n
				end
				else if( node_p > 0 && node_n < 1) begin
					node_p = node_p-1;
					c_volt = vector_x[ node_p ].getValue(); // get previous voltage value
					c_curr = (c_volt*2*elArray[i].getValue()/time_step) - (vector_b[ node_p ].getValue()); // get previous current value
					//$display("C_volt P: %f",  c_volt);
					//$display("C_curr P: %f",  c_curr);
					vector_trans[ elArray[i].getNodeP() ].addValue( 2*elArray[i].getValue()*c_volt/time_step + c_curr ); // stamp for node p
				end
				else if( node_n > 0 && node_p < 1) begin
					node_n = node_n-1;
					c_volt = -1*vector_x[ node_n ].getValue(); // get previous voltage value
					c_curr = (c_volt*2*elArray[i].getValue()/time_step) + (vector_b[ node_n ].getValue()); // get previous current value
					//$display("C_volt P: %f",  c_volt);
					//$display("C_curr P: %f",  c_curr);
					vector_trans[ elArray[i].getNodeP() ].addValue( 2*elArray[i].getValue()*c_volt/time_step + c_curr ); // stamp for node p
				end
			end
			INDUCTOR: begin
				node_p = size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName());
				if(node_p > 0) begin
					node_p = node_p - 1;
				end
				l_curr = vector_x[ node_p ].getValue(); // get previous current value
				l_volt = vector_b[ node_p ].getValue() + 2*elArray[i].getValue()*l_curr/time_step; // get previous voltage value // minus 1 and change to N
				
				vector_trans[ size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) ].addValue(-2*elArray[i].getValue()*l_curr/time_step + l_volt);
			end
			VS: begin 
				vector_trans[ size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) ].addValue(elArray[i].getValue());
			end
			CS: begin 
				vector_trans[ elArray[i].getNodeP() ].addValue(-1*elArray[i].getValue());
				vector_trans[ elArray[i].getNodeN() ].addValue(elArray[i].getValue());
			end
			default: begin
				end
		endcase
	end
	
	// copy vector_trans value to vector_b
	idx = 0;
	for(int i=1;i<(m_size);i++) begin
		//$display("vector trans: %f", vector_trans[i].getValue());
		vector_b[idx].setValue(vector_trans[i].getValue());
		//$display("vector b: %f", vector_b[idx].getValue());
		idx++;
	end
	
	// zeroed vector x and y
	for(int j=0;j<(matrix_size+1);j++) begin
		vector_x[j].setValue(0);
		vector_y[j].setValue(0);
	end
	
	//$display("\t# Updating vector b is finished #");
 endtask
 
/*********************************************************************************
**** Task for stamping elements into the matrix & vectors
**********************************************************************************/
  task stampElements();
	int size_min_vs;
	int idx; // index
	real id; // diode current
	real gd; // diode transconductance
	real d_cons; // diode 1/(n*vt)
	int node_p, node_n;
	//$display("## Start stamping elements... ##");
	
	node_p = 0;
	node_n = 0;
	id = 0.0;
	gd = 0.0;
	
	size_min_vs = matrix_size - vs_tracker.getNumbOfVS();
	// stamp elements in matrix A and vector B
	for(int i=0;i<(elArray.num());i++) begin
	//$display("Current Index: ", i);
	//elArray[i].printElementProperties();
	//$display("Element Name : %s", elArray[i].getName());
		case ( elArray[i].getType() )
		DIODE: begin 
				defineDiodeParamater(elArray[i].getVname()); // adjust diode parameter based on input model
				
				//only get value if node > 0. If node < 1, vd = 0
				node_p = elArray[i].getNodeP();
				node_n = elArray[i].getNodeN();
				
				if(node_p > 0 && node_n > 0) begin
					node_p = node_p-1;
					node_n = node_n-1;
					vd = vector_x[ node_p ].getValue() - vector_x[ node_n].getValue(); // get previous voltage value
				end
				else if( node_p > 0 && node_n < 1) begin
					node_p = node_p-1;
					vd = vector_x[ node_p ].getValue(); // get previous voltage value
				end
				else if( node_n > 0 && node_p < 1) begin
					node_n = node_n-1;
					vd = -1*vector_x[ node_n ].getValue(); // get previous voltage value
				end
				else begin
					vd = 0.0; // assign vd = 0 if node p/n = 0
				end
				
				d_cons = 1/(n*vt);
				id = i_sat*(exp**(vd*d_cons))-1; // calculate id
				gd = i_sat*d_cons*(exp**(vd*d_cons)); // calculate gd
				
				matrix_a[ (elArray[i].getNodeP()*matrix_size) + elArray[i].getNodeP() ].addValue( gd );
				matrix_a[ (elArray[i].getNodeP()*matrix_size) + elArray[i].getNodeN() ].addValue( -1*gd );
				matrix_a[ (elArray[i].getNodeN()*matrix_size) + elArray[i].getNodeP() ].addValue( -1*gd );
				matrix_a[ (elArray[i].getNodeN()*matrix_size) + elArray[i].getNodeN() ].addValue( gd );
				
				vector_b[ elArray[i].getNodeP() ].addValue( -1 * (id - vd*gd) );
				vector_b[ elArray[i].getNodeN() ].addValue( (id - vd*gd) );
				
			end
		RESISTOR: begin 
				matrix_a[ (elArray[i].getNodeP()*matrix_size) + elArray[i].getNodeP() ].addValue(1/elArray[i].getValue());
				matrix_a[ (elArray[i].getNodeP()*matrix_size) + elArray[i].getNodeN() ].addValue(-1/elArray[i].getValue());
				matrix_a[ (elArray[i].getNodeN()*matrix_size) + elArray[i].getNodeP() ].addValue(-1/elArray[i].getValue());
				matrix_a[ (elArray[i].getNodeN()*matrix_size) + elArray[i].getNodeN() ].addValue(1/elArray[i].getValue());
			end
		CAPACITOR: begin
				matrix_a[ (elArray[i].getNodeP()*matrix_size) + elArray[i].getNodeP() ].addValue(2*elArray[i].getValue()/time_step);
				matrix_a[ (elArray[i].getNodeP()*matrix_size) + elArray[i].getNodeN() ].addValue(-2*elArray[i].getValue()/time_step);
				matrix_a[ (elArray[i].getNodeN()*matrix_size) + elArray[i].getNodeP() ].addValue(-2*elArray[i].getValue()/time_step);
				matrix_a[ (elArray[i].getNodeN()*matrix_size) + elArray[i].getNodeN() ].addValue(2*elArray[i].getValue()/time_step);

				vector_b[ elArray[i].getNodeP() ].addValue( 2*elArray[i].getValue()*c_volt_temp/time_step + c_curr_temp );
				vector_b[ elArray[i].getNodeN() ].addValue( -2*elArray[i].getValue()*c_volt_temp/time_step + c_curr_temp );
		end
		INDUCTOR: begin
				matrix_a[ ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) )*matrix_size + elArray[i].getNodeP() ].addValue(1);
				matrix_a[ ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) )*matrix_size + elArray[i].getNodeN() ].addValue(-1);
				matrix_a[ (elArray[i].getNodeP()*matrix_size) + ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) ) ].addValue(1);
				matrix_a[ (elArray[i].getNodeN()*matrix_size) + ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) ) ].addValue(-1);
				
				matrix_a[ ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) )*matrix_size + ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) ) ].addValue(-2*elArray[i].getValue()/time_step);
				
				vector_b[ size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) ].addValue(-2*elArray[i].getValue()*l_curr_temp/time_step + l_volt_temp);
		end
		VS: begin 
				matrix_a[ ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) )*matrix_size + elArray[i].getNodeP() ].addValue(1);
				matrix_a[ ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) )*matrix_size + elArray[i].getNodeN() ].addValue(-1);
				matrix_a[ (elArray[i].getNodeP()*matrix_size) + ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) ) ].addValue(1);
				matrix_a[ (elArray[i].getNodeN()*matrix_size) + ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) ) ].addValue(-1);

				vector_b[ size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) ].addValue(elArray[i].getValue());
			end
		CS: begin 
				vector_b[ elArray[i].getNodeP() ].addValue(-1*elArray[i].getValue());
				vector_b[ elArray[i].getNodeN() ].addValue(elArray[i].getValue());
			end
		VCCS: begin 
				matrix_a[ (elArray[i].getNodeP()*matrix_size) + elArray[i].getNodeCP() ].addValue(elArray[i].getValue());
				matrix_a[ (elArray[i].getNodeP()*matrix_size) + elArray[i].getNodeCN() ].addValue(-1*elArray[i].getValue());
				matrix_a[ (elArray[i].getNodeN()*matrix_size) + elArray[i].getNodeCP() ].addValue(-1*elArray[i].getValue());
				matrix_a[ (elArray[i].getNodeN()*matrix_size) + elArray[i].getNodeCN() ].addValue(elArray[i].getValue());
			end
		VCVS: begin
				matrix_a[ ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) )*matrix_size + elArray[i].getNodeP() ].addValue(1);
				matrix_a[ ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) )*matrix_size + elArray[i].getNodeN() ].addValue(-1);
				matrix_a[ (elArray[i].getNodeP()*matrix_size) + ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) ) ].addValue(1);
				matrix_a[ (elArray[i].getNodeN()*matrix_size) + ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) ) ].addValue(-1);
				
				matrix_a[ ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) )*matrix_size + elArray[i].getNodeCP() ].addValue(-1*elArray[i].getValue());
				matrix_a[ ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) )*matrix_size + elArray[i].getNodeCN() ].addValue(elArray[i].getValue());
			end
		CCVS: begin
				matrix_a[ ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) )*matrix_size + elArray[i].getNodeP() ].addValue(1);
				matrix_a[ ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) )*matrix_size + elArray[i].getNodeN() ].addValue(-1);
				matrix_a[ (elArray[i].getNodeP()*matrix_size) + ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) ) ].addValue(1);
				matrix_a[ (elArray[i].getNodeN()*matrix_size) + ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) ) ].addValue(-1);
				
				matrix_a[ ( size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getName()) )*matrix_size + size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getVname()) ].addValue(-1*elArray[i].getValue());
			end
		CCCS: begin
				matrix_a[ (elArray[i].getNodeP()*matrix_size) + size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getVname()) ].addValue(elArray[i].getValue());
				matrix_a[ (elArray[i].getNodeN()*matrix_size) + size_min_vs + vs_tracker.getVsIndexByName(elArray[i].getVname()) ].addValue(-1*elArray[i].getValue());
			end
		default: begin
			end
		endcase
		
/*		$display("Printing Matrix A: ");
		for(int i=0;i<(matrix_size);i++) begin
			$write("\t[ ");
			for(int l=0;l<(matrix_size);l++) begin
				$write("%.6f", matrix_a[i*matrix_size + l].getValue() );
				$write("\t");
			end
			$write(" ]");
			$write("\n");
		end
		$write("\n");
*/	
	end
	
	// Remove 1st row and column of matrix A and vector B (no need for matrix X since all values are still 0's)
	// 	that's associated with gnd
	
	// reduce matrix a
	idx = 0;
	for(int i=1;i<(matrix_size);i++) begin
		for(int j=1;j<(matrix_size);j++) begin
			matrix_a[idx].setValue( matrix_a[i*matrix_size+j].getValue() );
			idx++;
		end
	end
	
	// reduce vector b
	idx = 0;
	for(int i=1;i<(matrix_size);i++) begin
		vector_b[idx].setValue( vector_b[i].getValue() );
		idx++;
	end
	
	matrix_size = matrix_size - 1; // reduce the size of the matrix and vectors
	
	//$display("## Stamping elements is done ##");

 endtask
 
/*********************************************************************************
**** Task for adjust diode parameter based on the input model
**********************************************************************************/
 task defineDiodeParamater(string dmodel);
 
	case (dmodel.toupper())
		"IN5711": begin
			i_sat = 0.00000000000001;
			n = 1;
		end
		"IN5711": begin
			i_sat = 0.000000315;
			n = 2.03;
		end
		"IN5712": begin
			i_sat = 0.000000000680;
			n = 1.003;
		end
		"IN34": begin
			i_sat = 0.000000000200;
			n = 2.19;
		end
		"IN4148": begin
			i_sat = 0.000000000035;
			n = 1.24;
		end
		"IN3891": begin
			i_sat = 0.000000063;
			n = 2;
		end
		"10A04": begin
			i_sat = 0.000000844;
			n = 2.06;
		end
		"IN4004": begin
			i_sat = 0.0000000769;
			n = 1.45;
		end
		default: begin
			 i_sat = 0.000000000001; // set default i_sat = 1p A
			vt = 0.026; // set Vt = 26mV as default
			exp = 2.71828; // exponent value (e)
			n = 1; // set default n = 1
		end
	endcase
	
 
 endtask
 
/*********************************************************************************
**** Task for performing LU decomposition
**********************************************************************************/
 task performLUFactorization();
	real sum;
	sum = 0;
	
	//$display("\t# Generating LU Matrix from matrix A... #");
	// Generate LU matrix from matrix A using Crout algorithm
	// Note: Crout uses unit diagonals for the upper triangle
   for(int k=0; k<matrix_size; ++k) begin
		for(int i=k; i<matrix_size; ++i) begin
			sum = 0;
			for(int p=0;p<k;++p) begin
				sum += matrix_lu[ i*matrix_size+p ].getValue() * matrix_lu[ p*matrix_size+k ].getValue();
			end
			matrix_lu[ i*matrix_size+k ].setValue( matrix_a[ i*matrix_size+k ].getValue() - sum ); // not dividing by diagonals
			matrix_lu[ i*matrix_size+k ].setValue( matrix_a[ i*matrix_size+k ].getValue() - sum ); // not dividing by diagonals
		end
		for(int j=k+1; j<matrix_size; ++j) begin
			sum = 0;
			for(int p=0; p<k; ++p) begin
				sum += matrix_lu[ k*matrix_size+p ].getValue() * matrix_lu[ p*matrix_size+j ].getValue();
			end
			matrix_lu[ k*matrix_size+j ].setValue( ( matrix_a[ k*matrix_size+j ].getValue()-sum ) / matrix_lu[ k*matrix_size+k ].getValue() );
        end
   end
   //$display("\t# Generating LU Matrix is done #");

 endtask
 
/*********************************************************************************
**** Task for solving linear equation system Ax=b using LU decomposition
**********************************************************************************/
 task solveAxBWithLUDecomposition();
   real sum;
	sum = 0;
  //$display("## Solving linear system Ax=B... ##");
 
  //$display("\t# Solving Ly=B... #");
	// Solve Ax=B using LU matrix
	// Solve Ly=B
   for(int i=0; i<matrix_size; ++i) begin
		sum = 0;
		for(int k=0; k<i; ++k) begin
			sum += matrix_lu[i*matrix_size+k].getValue() * vector_y[k].getValue();
		end
		vector_y[i].setValue( (vector_b[i].getValue() - sum)/matrix_lu[ i*matrix_size+i ].getValue() );
   end
   //$display("\t# Solving Ly=B is done #");
   
   //$display("\t# Solving Ux=B... #");
   // Solve Ux=Y
   for(int i=matrix_size-1; i>=0; --i) begin
		sum = 0;
		for(int k=i+1;k<matrix_size;++k) begin
			sum += matrix_lu[i*matrix_size+k].getValue() * vector_x[k].getValue();
		end
		vector_x[i].setValue((vector_y[i].getValue() - sum ) ); // not dividing by diagonals
   end
   //$display("\t# Solving Ux=B is done #");
   
   //$display("## Solving linear system Ax=B is done... ##");
   
 endtask

/*********************************************************************************
**** Task for putting elements at one line into an element buffer
**** @param: 
****		string inputStr - a string that want to be parsed
****		ref string elementInALineArray[int] - reference to an element array for
****			storing the results
**********************************************************************************/
 task automatic getElements(string inputStr, ref string elementInALineArray[int]); 
	//string elementInALineArray[int];
	int stopIndex;
	int startIndex;
	int arrayIndex;
	
	arrayIndex = 0;
	startIndex = 0;
	stopIndex = 0;
	
	//$display("Input String: %s", inputStr);
	while (stopIndex < inputStr.len()) begin
		//$display("Char at sindex %s", inputStr.getc(0));
		//$display("Char at start index: %c", inputStr.getc(startIndex));
		//$display("Char at stop index: %c", inputStr.getc(stopIndex));
		while (inputStr.getc(stopIndex) != " " && inputStr.getc(stopIndex) != "\n" ) begin
			stopIndex++;
		end
			
		//$display("Char at start index: %c", inputStr.getc(startIndex));
		//$display("Char at stop index: %c", inputStr.getc(stopIndex-1));
			
		elementInALineArray[arrayIndex] = inputStr.substr(startIndex, stopIndex-1);
		stopIndex++;
		arrayIndex++;
		startIndex = stopIndex;
	end
	//$display("ElementBuff Size: %d", elementInALineArray.num());
	//$display("Exit the loop");
	
	
 endtask
 
 /*********************************************************************************
**** Function for converting node to integer
**** @param: 
****		string node_s - node in string
**** @return: 
****		int node_i - node in integer
**********************************************************************************/
 function int convertNodeToInt(string node_s);
	int node_i;
	
	case (node_s.toupper())
		"A": begin
			node_i = 1;
		end
		"B": begin
			node_i = 2;
		end
		"C": begin
			node_i = 3;
		end
		"D": begin
			node_i = 4;
		end
		"E": begin
			node_i = 5;
		end
		"F": begin
			node_i = 6;
		end
		"G": begin
			node_i = 7;
		end
		"H": begin
			node_i = 8;
		end
		"I": begin
			node_i = 9;
		end
		"J": begin
			node_i = 10;
		end
		"K": begin
			node_i = 11;
		end
		"L": begin
			node_i = 12;
		end
		"M": begin
			node_i = 13;
		end
		"N": begin
			node_i = 14;
		end
		"O": begin
			node_i = 15;
		end
		"P": begin
			node_i = 16;
		end
		"Q": begin
			node_i = 17;
		end
		"R": begin
			node_i = 18;
		end
		"S": begin
			node_i = 19;
		end
		"T": begin
			node_i = 20;
		end
		"U": begin
			node_i = 21;
		end
		"V": begin
			node_i = 22;
		end
		"W": begin
			node_i = 23;
		end
		"X": begin
			node_i = 24;
		end
		"Y": begin
			node_i = 25;
		end
		"Z": begin
			node_i = 26;
		end
		"%": begin
			node_i = -1;
			throwErrorMsgAndExit("Node string is not valid. Make sure the node string is either of these char: a-z or positive integers");
		end
		default: begin
			node_i = node_s.atoi();
		end
	endcase
	
	return node_i;
	
 endfunction
 
 
/*********************************************************************************
**** Task for parsing a netlist file
**** @param: 
****		string file_path - path to a file
**********************************************************************************/
 task netlistParser(string file_path);
	integer file, c;
	integer buff_idx;
	integer elArray_idx;
	string str;
	string element_buff[int];
	string str_real;
	string first_char;
	int endoffile;
	
	// open input file
	file = $fopen(file_path,"r");
	if(file == `NULL) begin
		throwErrorMsgAndExit("Can't open the file");
	end
	
	$display("## Buffering the input file... ##");
	c = 255;
	buff_idx = 0;
	while( c > 0 ) begin
		c = $fgets(str, file);
		//$display("Str: %s", str);
		//$display("Char: %d", c);
		if( c > 2 && c != 0 ) begin // don't buffer whitespace
			netlist_buff[buff_idx] = str;
			buff_idx++;
		end
	end
	$display("## Buffering input file is done ##");
	$fclose(file); // closing input file 
	
	//displayBufferedNetList(); // print buffered netlist
	
	$display("## Parsing netlist... ##");
	buff_idx = 0;
	elArray_idx = 0;
	endoffile = 0;
	while( !endoffile ) begin
		//$display(netlist_buff[buff_idx].icompare(".END\n"));
		//$display(netlist_buff[buff_idx]);
		
		// start parsing each line
		getElements(netlist_buff[buff_idx], element_buff); // put all words at current line in a buffer
		
		first_char = netlist_buff[buff_idx].substr(0, 0); // get first char at current line
		first_char = first_char.toupper();
		//$display("First char: %s", first_char);
		case ( first_char )
			"D" : begin
				//$display("ElBuff name: %s", element_buff[0]);
				//$display("ElBuff n1: %d", convertNodeToInt(element_buff[1]));
				//$display("ElBuff n2: %d", convertNodeToInt(element_buff[2]));
				//$display("ElBuff model name: %s", element_buff[3]); // store diode model in vname field
				
				elmnts = new(element_buff[0], DIODE, convertNodeToInt(element_buff[1]), convertNodeToInt(element_buff[2]), -1, -1, 0.0, element_buff[3]);
				elArray[elArray_idx] = elmnts;
				updateMaxNode(); // update the highest node number
				elArray_idx++;
			end
			"R" : begin
				//$display("ElBuff name: %s", element_buff[0]);
				//$display("ElBuff n1: %d", convertNodeToInt(element_buff[1]));
				//$display("ElBuff n2: %d", convertNodeToInt(element_buff[2]));
				str_real = {element_buff[3],"0"}; // add additional number for element value since atoreal() always skips the last digit
				//$display("ElBuff value: %f", str_real.atoreal());
				
				elmnts = new(element_buff[0], RESISTOR, convertNodeToInt(element_buff[1]), convertNodeToInt(element_buff[2]), -1, -1, str_real.atoreal(), "NA");
				elArray[elArray_idx] = elmnts;
				updateMaxNode(); // update the highest node number
				elArray_idx++;
			end
			"C" : begin
				//$display("ElBuff name: %s", element_buff[0]);
				//$display("ElBuff n1: %d", convertNodeToInt(element_buff[1]));
				//$display("ElBuff n2: %d", convertNodeToInt(element_buff[2]));
				str_real = {element_buff[3],"0"}; // add additional number for element value since atoreal() always skips the last digit
				//$display("ElBuff value: %f", str_real.atoreal());
				
				elmnts = new(element_buff[0], CAPACITOR, convertNodeToInt(element_buff[1]), convertNodeToInt(element_buff[2]), -1, -1, str_real.atoreal(), "NA");
				elArray[elArray_idx] = elmnts;
				updateMaxNode(); // update the highest node number
				elArray_idx++;
			end
			"L" : begin
				//$display("ElBuff name: %s", element_buff[0]);
				//$display("ElBuff n1: %d", convertNodeToInt(element_buff[1]));
				//$display("ElBuff n2: %d", convertNodeToInt(element_buff[2]));
				str_real = {element_buff[3],"0"}; // add additional number for element value since atoreal() always skips the last digit
				//$display("ElBuff value: %f", str_real.atoreal());
				
				elmnts = new(element_buff[0], INDUCTOR, convertNodeToInt(element_buff[1]), convertNodeToInt(element_buff[2]), -1, -1, str_real.atoreal(), "NA");
				elArray[elArray_idx] = elmnts;
				updateMaxNode(); // update the highest node number
				elArray_idx++;
				vs_tracker.addVS(element_buff[0], elArray_idx); // add inductor (or "voltage source") into voltage source tracker
			end
			"V" : begin
				//$display("ElBuff name: %s", element_buff[0]);
				//$display("ElBuff n1: %d", convertNodeToInt(element_buff[1]));
				//$display("ElBuff n2: %d", convertNodeToInt(element_buff[2]));
				str_real = {element_buff[3],"0"}; // add additional number for element value since atoreal() always skips the last digit
				//$display("ElBuff value: %f", str_real.atoreal());
				
				elmnts = new(element_buff[0], VS, convertNodeToInt(element_buff[1]), convertNodeToInt(element_buff[2]), -1, -1, str_real.atoreal(), "NA");
				elArray[elArray_idx] = elmnts;
				updateMaxNode(); // update the highest node number
				elArray_idx++;
				vs_tracker.addVS(element_buff[0], elArray_idx); // add voltage source into voltage source tracker
			end
			"I" : begin
				//$display("ElBuff name: %s", element_buff[0]);
				//$display("ElBuff n1: %d", convertNodeToInt(element_buff[1]));
				//$display("ElBuff n2: %d", convertNodeToInt(element_buff[2]));
				str_real = {element_buff[3],"0"}; // add additional number for element value since atoreal() always skips the last digit
				//$display("ElBuff value: %f", str_real.atoreal());
				
				elmnts = new(element_buff[0], CS, convertNodeToInt(element_buff[1]), convertNodeToInt(element_buff[2]), -1, -1, str_real.atoreal(), "NA");
				elArray[elArray_idx] = elmnts;
				updateMaxNode(); // update the highest node number
				elArray_idx++;
			end
			"G" : begin
				//$display("ElBuff name: %s", element_buff[0]);
				//$display("ElBuff n1: %d", convertNodeToInt(element_buff[1]));
				//$display("ElBuff n2: %d", convertNodeToInt(element_buff[2]));
				//$display("ElBuff nc1: %d", convertNodeToInt(element_buff[3]));
				//$display("ElBuff nc2: %d", convertNodeToInt(element_buff[4]));
				str_real = {element_buff[5],"0"}; // add additional number for element value since atoreal() always skips the last digit
				//$display("ElBuff value: %f", str_real.atoreal());
				
				elmnts = new(element_buff[0], VCCS, convertNodeToInt(element_buff[1]), convertNodeToInt(element_buff[2]), convertNodeToInt(element_buff[3]), convertNodeToInt(element_buff[4]), str_real.atoreal(), "NA");
				elArray[elArray_idx] = elmnts;
				updateMaxNode(); // update the highest node number
				elArray_idx++;
			end
			"E" : begin
				//$display("ElBuff name: %s", element_buff[0]);
				//$display("ElBuff n1: %d", convertNodeToInt(element_buff[1]));
				//$display("ElBuff n2: %d", convertNodeToInt(element_buff[2]));
				//$display("ElBuff nc1: %d", convertNodeToInt(element_buff[3]));
				//$display("ElBuff nc2: %d", convertNodeToInt(element_buff[4]));
				str_real = {element_buff[5],"0"}; // add additional number for element value since atoreal() always skips the last digit
				//$display("ElBuff value: %f", str_real.atoreal());
				
				elmnts = new(element_buff[0], VCVS, convertNodeToInt(element_buff[1]), convertNodeToInt(element_buff[2]),  convertNodeToInt(element_buff[3]), convertNodeToInt(element_buff[4]), str_real.atoreal(), "NA");
				elArray[elArray_idx] = elmnts;
				updateMaxNode(); // update the highest node number
				elArray_idx++;
				vs_tracker.addVS(element_buff[0], elArray_idx); // add voltage source into voltage source tracker
			end
			"F" : begin
				//$display("ElBuff name: %s", element_buff[0]);
				//$display("ElBuff n1: %d", convertNodeToInt(element_buff[1]));
				//$display("ElBuff n2: %d", convertNodeToInt(element_buff[2]));
				//$display("ElBuff vname: %s", element_buff[3]);
				str_real = {element_buff[4],"0"}; // add additional number for element value since atoreal() always skips the last digit
				//$display("ElBuff value: %f", str_real.atoreal());
				
				elmnts = new(element_buff[0], CCCS, convertNodeToInt(element_buff[1]), convertNodeToInt(element_buff[2]), -1, -1, str_real.atoreal(), element_buff[3]);
				elArray[elArray_idx] = elmnts;
				updateMaxNode(); // update the highest node number
				elArray_idx++;
			end
			"H" : begin
				//$display("ElBuff name: %s", element_buff[0]);
				//$display("ElBuff n1: %d", convertNodeToInt(element_buff[1]));
				//$display("ElBuff n2: %d", convertNodeToInt(element_buff[2]));
				//$display("ElBuff vname: %s", element_buff[3]);
				str_real = {element_buff[4],"0"}; // add additional number for element value since atoreal() always skips the last digit
				//$display("ElBuff value: %f", str_real.atoreal());
				
				elmnts = new(element_buff[0], CCVS, convertNodeToInt(element_buff[1]), convertNodeToInt(element_buff[2]), -1, -1, str_real.atoreal(), element_buff[3]);
				elArray[elArray_idx] = elmnts;
				updateMaxNode(); // update the highest node number
				elArray_idx++;
				vs_tracker.addVS(element_buff[0], elArray_idx); // add voltage source into voltage source tracker
			end
			"*" : begin
				//$display("Comment card. Do Nothing here");
			end
			"." : begin
				//$display(element_buff[0]);

				if( element_buff[0].substr(0,3) == ".END" ||
					element_buff[0].substr(0,3) == ".end") begin
					$display("\tReaching end of file");
					endoffile = 1;
				end
				else if( element_buff[0].substr(0,2) == ".OP" ||
					element_buff[0].substr(0,2) == ".op") begin
					//$display("Action OP detected");
					action[action_index++] = OP;
				end
				else if( element_buff[0].substr(0,2) == ".DC" ||
					element_buff[0].substr(0,2) == ".dc") begin
					//$display("Action dc detected");
					action[action_index++] = DC;
				end
				else if( element_buff[0].substr(0,7) == ".NETLIST" ||
					element_buff[0].substr(0,7) == ".netlist") begin
					//$display("Action netlist detected");
					action[action_index++] = NETLIST;
				end
				else if( element_buff[0].substr(0,4) == ".TRAN" ||
					element_buff[0].substr(0,4) == ".tran") begin
					//$display("Action trans detected");
					action[action_index++] = TRAN;
					
					// get time step and time max from user - currently disabled for this project
					/*
					str_real = {element_buff[1],"0"};
					time_step = str_real.atoreal(); //get time step
					str_real = {element_buff[2],"0"};
					time_max = str_real.atoreal(); //get time max
					*/
					
					//$display("Time Step: %f", time_step);
					//$display("Time Max: %f", time_max);
				end
				else begin
					// do nothing
					//$display("Default");
				end
			end
			" " : begin
				// Skip white spaces
			end
		default: begin
				throwErrorMsgAndExit("Can't parse netlist. Please check you netlist format again.");
			end
		endcase
		
		buff_idx++; // incremenet line buffer index
	end
	
	matrix_size = max_node + vs_tracker.getNumbOfVS() + 1; // let matrix size = the highest number of node
	
	$display("## Parsing netlist is done ##");
	
 endtask
 
/*********************************************************************************
**** Task for updating the maximum node number
**********************************************************************************/
 task updateMaxNode();
	// keep tracking max node for getting the right matrix size
	if ( max_node < elmnts.getMaxNode()) begin
			max_node = elmnts.getMaxNode();

	end
 endtask
 
/*********************************************************************************
**** Task for initializing matrices and vectors
**********************************************************************************/
  task initMatrixAndVectors();
	$display("## Initializing matrices... ##");
	
	// construct the matrix A using dense format
	for(int i=0;i<(matrix_size * matrix_size);i++) begin
		stmp = new();
		stmp.setValue(0.0);
		matrix_a[i] = stmp;
	end
	
	// construct the matrix LU using dense format
	for(int i=0;i<(matrix_size*matrix_size);i++) begin
		stmp = new();
		stmp.setValue(0.0);
		matrix_lu[i] = stmp;
	end
	
	// construct the vector X using dense format
	for(int j=0;j<(matrix_size);j++) begin
		stmp = new();
		stmp.setValue(0.0);
		vector_x[j] = stmp;
	end
	
	// construct the vector B using dense format
	for(int k=0;k<(matrix_size);k++) begin
		stmp = new();
		stmp.setValue(0.0);
		vector_b[k] = stmp;
	end
	
	// construct the vector Y using dense format
	for(int j=0;j<(matrix_size);j++) begin
		stmp = new();
		stmp.setValue(0.0);
		vector_y[j] = stmp;
	end
	
	$display("## Matrix A and vectors X & B have been created... ##");
	
 endtask
 
/*********************************************************************************
**** Task for resetting matrices and vectors values back to zeros except vector x
**********************************************************************************/
 task resetMatrixAndVectors();
	$display("## Resetting matrices... ##");
		
	// reset matrix A & LU 
	for(int i=0;i<(matrix_size*matrix_size);i++) begin
		matrix_lu[i].setValue(0);
		matrix_a[i].setValue(0);
	end
	
	//reset vector B
	for(int k=0;k<(matrix_size);k++) begin
		vector_b[k].setValue(0);
	end
	
	// reset vector X & Y
	for(int j=0;j<(matrix_size);j++) begin
		vector_x[j].setValue(0);
		vector_y[j].setValue(0);
	end
	
	
	
	$display("## Matrix A and vectors X & B have been reset... ##");
	
 endtask
 
 
/*********************************************************************************
**** Task for throwing error message and terminate program
**** @param: 
****		string error_msg - error message that will be displayed before exiting
**********************************************************************************/
task throwErrorMsgAndExit(string error_msg);
	$write("!! ERROR: "); 
	$write(error_msg); 
	$write(" !!\n"); 
	$display("Exiting program...");
	
	#1 $finish;

endtask

/*********************************************************************************
**** Task for displaying buffered netlist
**********************************************************************************/
 task displayBufferedNetList();
	// print buffered netlist
	$display("Buffered Netlist: ");
	for(int i=0;i<netlist_buff.num();i++) begin
		$write("\t");
		$write(netlist_buff[i]);
		$write("\n");
	end
 endtask
 
/*********************************************************************************
**** Task for displaying element array
**********************************************************************************/
 task displayElementArray();
	// printing all elements in the element array
	$display("Element Array Data: ");
	for(int j=0;j<elArray.num();j++) begin
		elArray[j].printElementProperties();
		$write("\n");
	end
 
 endtask

/*********************************************************************************
**** Task for displaying all matrices and vectors
**********************************************************************************/
 task displayMatrixAndVectors();
	$display("\n+------------------------- Linear System Solver Summary -------------------------+\n"); 
	// prints all the matrix & vectors
	$display("Printing Matrix A: ");
	for(int i=0;i<(matrix_size);i++) begin
			$write("\t[ ");
		for(int l=0;l<(matrix_size);l++) begin
			$write("%.6f", matrix_a[i*matrix_size + l].getValue() );
			$write("\t");
		end
			$write(" ]");
			$write("\n");
	end
	$write("\n");
	
	$display("Printing Vector B: ");
	for(int k=0;k<(matrix_size);k++) begin
		$write("\t[ ");
		$write("%.6f", vector_b[k].getValue() );
		$write(" ]\n");
	end
	$write("\n");
	
	$display("Printing Matrix LU: ");
	for(int i=0;i<(matrix_size);i++) begin
		$write("\t[ ");
		for(int l=0;l<(matrix_size);l++) begin
			$write("%.6f", matrix_lu[i*matrix_size + l].getValue() );
			$write("\t");
		end
		$write(" ]\n");
	end
	$write("\n");
	
	$display("Printing Vector Y: ");
	for(int k=0;k<(matrix_size);k++) begin
		$write("\t[ ");
		$write("%.6f", vector_y[k].getValue() );
		$write(" ]\n");
	end
	$write("\n");
		
	$display("Printing Vector X: ");
	for(int j=0;j<(matrix_size);j++) begin
		$write("\t[ ");
		$write("%.6f", vector_x[j].getValue() );
		$write(" ]\n");
	end
	$write("\n");
	
	$display("+----------------------------- ++++++++++++++++++ -----------------------------+\n"); 
 
 endtask

/*********************************************************************************
**** Task for displaying DC analysis report
**********************************************************************************/
task displayDCAnalysisReport();
	$display("\n+------------------------- DC Analysis Report -------------------------+\n"); 
	// print vector X values (voltage only)
	$display("DC Operating Voltages: ");
	$display("\tNode\tVoltage Lvl"); 
	for(int k=0;k<(matrix_size-vs_tracker.getNumbOfVS());k++) begin
		$write("%d:", k+1);
		$write("\t%.6f V", vector_x[k].getValue());
		$write("\n");
	end
	$write("\n");
	// print vector X values (current only)
	$display("DC Operating Currents: ");
	$display("\tComponent\tCurrent"); 
	for(int k=(matrix_size-vs_tracker.getNumbOfVS());k<matrix_size;k++) begin
		$write("\t\t%s:", vs_tracker.getNameByIndex(k-(matrix_size-vs_tracker.getNumbOfVS())) );
		$write("\t%.4f A", vector_x[k].getValue());
		$write("\n");
	end
	$write("\n");
	$display("+------------------------- ++++++++++++++++++ -------------------------+\n"); 
	
endtask

/*********************************************************************************
**** Task for printing buffered netlist to a file
**********************************************************************************/
 task printBufferedNetList(integer f);
	$fwrite(f, "\n+--------------------------------- Netlist ----------------------------------+\n\n"); 
	// print buffered netlist
	$fwrite(f, "Netlist from %s: \n", netlist_path);
	for(int i=0;i<netlist_buff.num();i++) begin
		$fwrite(f, "\t");
		$fwrite(f, netlist_buff[i]);
	end
	$fwrite(f, "\n");
	
	$fwrite(f, "+----------------------------- ++++++++++++++++++ -----------------------------+\n\n"); 
	
 endtask

/*********************************************************************************
**** Task for printing all matrices and vectors to a file
**********************************************************************************/
 task printMatrixAndVectorsToAFile(integer f);
	$fwrite(f, "\n+------------------------- Linear System Solver Summary -------------------------+\n\n"); 
	// prints all the matrix & vectors
	$fwrite(f, "Matrix A: \n");
	for(int i=0;i<(matrix_size);i++) begin
			$fwrite(f, "\t[ ");
		for(int l=0;l<(matrix_size);l++) begin
			$fwrite(f, "%.6f", matrix_a[i*matrix_size + l].getValue() );
			$fwrite(f, "\t");
		end
			$fwrite(f, " ]");
			$fwrite(f, "\n");
	end
	$fwrite(f,"\n");
	
	$fwrite(f, "Vector B: \n");
	for(int k=0;k<(matrix_size);k++) begin
		$fwrite(f, "\t[ ");
		$fwrite(f, "%.6f", vector_b[k].getValue() );
		$fwrite(f, " ]\n");
	end
	$fwrite(f, "\n");
	
	$fwrite(f, "Matrix LU: \n");
	for(int i=0;i<(matrix_size);i++) begin
		$fwrite(f, "\t[ ");
		for(int l=0;l<(matrix_size);l++) begin
			$fwrite(f, "%.6f", matrix_lu[i*matrix_size + l].getValue() );
			$fwrite(f, "\t");
		end
		$fwrite(f, " ]\n");
	end
	$fwrite(f, "\n");
	
	$fwrite(f, "Vector Y: \n");
	for(int k=0;k<(matrix_size);k++) begin
		$fwrite(f, "\t[ ");
		$fwrite(f, "%.6f", vector_y[k].getValue() );
		$fwrite(f, " ]\n");
	end
	$fwrite(f, "\n");
		
	$fwrite(f, "Vector X: \n");
	for(int j=0;j<(matrix_size);j++) begin
		$fwrite(f, "\t[ ");
		$fwrite(f, "%.6f", vector_x[j].getValue() );
		$fwrite(f, " ]\n");
	end
	$fwrite(f, "\n");
	
	$fwrite(f, "+----------------------------- ++++++++++++++++++ -----------------------------+\n\n"); 
 
 endtask

/*********************************************************************************
**** Task for printing DC analysis report to a file
**********************************************************************************/
task printDCAnalysisReportToAFile(integer f);	
	$display("Printing DC analysis report to a file...");

	$fwrite(f, "\n+------------------------- DC Analysis Report -------------------------+\n\n"); 
	// print vector X values (voltage only)
	$fwrite(f, "DC Operating Voltages: \n");
	$fwrite(f, "\tNode\tVoltage Lvl\n"); 
	for(int k=0;k<(matrix_size-vs_tracker.getNumbOfVS());k++) begin
		$fwrite(f, "%d:", k+1);
		$fwrite(f, "\t%.6f V", vector_x[k].getValue());
		$fwrite(f, "\n");
	end
	$fwrite(f, "\n");
	// print vector X values (current only)
	$fwrite(f, "DC Operating Currents: \n");
	$fwrite(f, "\tComponent\tCurrent\n"); 
	for(int k=(matrix_size-vs_tracker.getNumbOfVS());k<matrix_size;k++) begin
		$fwrite(f, "\t\t%s:", vs_tracker.getNameByIndex(k-(matrix_size-vs_tracker.getNumbOfVS())) );
		$fwrite(f, "\t%.4f A", vector_x[k].getValue());
		$fwrite(f, "\n");
	end
	$fwrite(f, "\n");
	$fwrite(f, "+------------------------- ++++++++++++++++++ -------------------------+\n\n"); 	

	$display("Printing DC analysis report to a file is done");
	
endtask

