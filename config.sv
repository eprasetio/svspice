/*********************************************************************************
****	iSPICE EE 537: config.sv
****	-- Configuration file --
****
****	by Edwin Prasetio
****
**** This is a config file where user can specify netlist path, output file path,
****	and both time step and time max for transient analysis
****
**********************************************************************************/
  
  // define input file path here
  //string netlist_path = "./benchmark/representative.txt";
  string netlist_path = "./benchmark/rc.txt";
  
  // other test netlists - for testing purposes
  //string netlist_path = "./benchmark/netlist.txt";
  //string netlist_path = "./benchmark/netlist2.txt";
  //string netlist_path = "./benchmark/netlist3.txt"; 
  //string netlist_path = "./benchmark/netlist4.txt"; 
  //string netlist_path = "./benchmark/netlist5.txt"; 
  //string netlist_path = "./benchmark/netlist6.txt"; 
  //string netlist_path = "./benchmark/netlist7.txt"; 
  
  // define output file name & path here
  //string output_file_path = "iSPICE_REP_Result.txt";
  string output_file_path = "iSPICE_RC_Result.txt";
  //string output_file_path = "iSPICE_DIODE_Result.txt";
  
  // define time step and time max for transient analysis
  real time_step = 0.0000001; // set default time step: 100ns
  real time_max =  0.000015;  // set max time step: 15us
  
  // define diode default parameter
  real i_sat = 0.000000000001; // set default i_sat = 1p A
  real vt = 0.026; // set Vt = 26mV as default
  real exp = 2.71828; // exponent value (e)
  real n = 1; // set default n = 1