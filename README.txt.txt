/*********************************************************************************
****	iSPICE EE 537: README.txt
****
****	by Edwin Prasetio
****
**** This .txt file contains instruction on how to run iSPICE program
****
**********************************************************************************/

Instruction to use iSPICE System Verilog Version:
1. Specify input netlist
	Open config.sv file, then edit netlist_path variable value with your new netlist's file path  
   
	Ex: string netlist_path = "./benchmark/rc.txt";

2. Running DC analysis
	Put .op keyword in your netlist to tell iSPICE to run DC analysis.
	Then, in the terminal window, run ispice.script to run the program (Type "./ispice.script"). 
	Make sure you are in the same directory where ispice files are located.
    
3. Running TRAN analysis
	Put .tran keyword in your netlist to tell iSPICE to run TRAN analysis.
	To change the default time step (100ns) and time max (15us), open config.sv file, and then edit time_step and time_max variable to the desired values.
	
	Ex: real time_step = 0.0000001; // set default time step: 100ns
	    real time_max =  0.000015;  // set max time step: 15us
	
	Then, in the terminal window, run ispice.script to run the program (Type "./ispice.script"). 
	Make sure you are in the same directory where ispice files are located.

4. Specify output file name and path
	Open config.sv file, then edit output_file_path variable value with your desired output file path  
   
	Ex: string output_file_path = "iSPICE_REP_Result.txt";

	
Notes for current iSPICE version:
- Make sure to put .end at the end of your netlist. This keyword is used by iSPICE to tell the end of the netlist file.
- This version supports letter format nodes. Also, the program can handle both upper and lower letter keyword
- Specifying time steps and max from netlist are currently disabled. To enable this, open taskLib.sv file, go to netlistParser() task,
  and uncomment the following code block:
					// get time step and time max from user - currently disabled for this project
					/*
					str_real = {element_buff[1],"0"};
					time_step = str_real.atoreal(); //get time step
					str_real = {element_buff[2],"0"};
					time_max = str_real.atoreal(); //get time max
					*/  
- Supports Diode, Resistor, Capacitor, Inductor, Current Source, Voltage Source, VCVS, VCCS, CCCS, and CCVS
- Support DC and TRAN analysis

 