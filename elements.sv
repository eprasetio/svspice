/*********************************************************************************
****	iSPICE EE 537: elements.sv
**** -- Element Class for storing supported netlist elements --
****
****	by Edwin Prasetio
****
**** Elements class currently supports: Resistor, voltage source (VS), 
**** 	current source(IS), voltage controlled current source (VCCS),
****	voltage controlled voltage source (VCVS), current controlled current source (CCCS),
****	and current controlled voltage source (CCVS)
****
**********************************************************************************/

class Elements;
	// variable for returning error 
	string NA_S = "NA";
	int NA_I = -1;
	
 	string name;
    real value;
    int elementType;
    int node_p;
    int node_n;
	int nc_p;
    int nc_n;
	string vname;
	
	// Class constructor
     function new(string input_name, int input_type, int input_node_p,
      	int input_node_n, int input_nc_p, int input_nc_n, real input_val
		, string input_vname);
          name = input_name;
          elementType = input_type;
          node_p = input_node_p;
          node_n = input_node_n;
          value = input_val;
		  nc_p = input_nc_p;
		  nc_n = input_nc_n;
		  vname = input_vname;
      endfunction
      
	  // Get element type
      function int getType();
          return elementType;
      endfunction
		
	  // Get value
      function real getValue();
          return value;
      endfunction
	  
	  // Get name
      function string getName();
          return name;
      endfunction
	  
	  // Get max node
	  function int getMaxNode();
		if ( (node_p > node_n) && (nc_p > nc_n) && (node_p > nc_p) ) begin
			return node_p;
		end
		else if ( (node_p > node_n) && (nc_p > nc_n) && (node_p < nc_p) ) begin
			return nc_p;
		end
		else if ( (node_p < node_n) && (nc_p < nc_n) && (node_n < nc_n) ) begin
			return nc_n;
		end
		else begin
			return node_n;
		end
      endfunction
	  
	  // Get node p
      function int getNodeP();
          return node_p;
      endfunction

	  // Get node n
      function int getNodeN();
          return node_n;
      endfunction
	  
	  // Get nodeC p
	  function int getNodeCP();
		if(elementType == VCCS || elementType == VCVS) begin
          return nc_p;
		end 
		else begin
		  return NA_I;
		end
      endfunction

	  // Get nodeC n
      function int getNodeCN();
		if(elementType == VCCS || elementType == VCVS) begin
          return nc_n;
		end 
		else begin
		  return NA_I;
		end
      endfunction
	  
	  // Get vname
      function string getVname();
          if(elementType == CCCS || elementType == CCVS) begin
          return vname;
		end 
		else begin
		  return NA_S;
		end
      endfunction
	  
	  // Print element properties
	  task printElementProperties();
		$write("\tName: %s\n", name);
		$write("\tValue: %.6f\n", value);
		$write("\tType: %d\n", elementType);
		$write("\tNodeP: %d\n", node_p);
		$write("\tNodeC: %d\n", node_n);
		$write("\tNodeC_P: %d\n", nc_p);
		$write("\tNodeC_N: %d\n", nc_n);
		$write("\tVName: %s\n", vname);
		
	  endtask
	  
endclass