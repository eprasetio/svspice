/*********************************************************************************
****	iSPICE EE 537: vstracker.sv
****	-- Class that store and tracks number of voltage source in the circuit --
****
****	by Edwin Prasetio
****
**** Since voltage source increases the dimension of the linear system matrices &
****	vectors (when using MNA), I built this class to keep in track the number 
****	of voltage source in the circuit. Also, this class enables faster voltage 
****	source searching in the data structure
****
**********************************************************************************/

class VSTracker;

    string vs[int];
	int location[int];
	int index;
	
	// Class constructor
     function new();
		  index = 0;
      endfunction
      
	  // Add new voltage source and its location in elArray
      task addVS(string input_vs, int input_location);
		vs[index] = input_vs;
		location[index] = input_location;
		index++;
      endtask
		
	  // Get number of voltage source in the circuit 
      function int getNumbOfVS();
          return vs.num();
      endfunction
	  
	  // search voltage source location by name. Assume no vs name duplicate here
	  function int getVsLocationByName(string name);
		for(int i=0;i<vs.num();i++) begin
			if( vs[i] == name) begin
				return location[i];
			end
		end
	  endfunction
	  
	  // get voltage source index by name
	  function int getVsIndexByName(string name);
		for(int i=0;i<vs.num();i++) begin
			if( vs[i] == name) begin
				return i;
			end
		end
	  endfunction
	  
	  // get voltage source name by index
	  function string getNameByIndex(int index);
		return vs[index];
	  endfunction
	  
	  // Print all voltage source in the tracker
	  task printVsTracker();
		$display("Voltage sources in the tracker: ");
		for(int i=0;i<vs.num();i++) begin
			$display("VS #%d: %s, stored at location %d", index, vs[i], location[i]);
		end
	  endtask
	  
endclass