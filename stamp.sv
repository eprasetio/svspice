/*********************************************************************************
****	iSPICE EE 537: stamp.sv
****	-- A Stamp class that define the variable types in the Ax=b matrices --
****
****	by Edwin Prasetio
****
**** I created this class because currently SystemVerilog has some issues in 
**** 	supporting multidimensional array with real type
****
**********************************************************************************/

class Stamp;

    real value; // The stamp class is actually a real value
	
	// Class constructor
     function new();
          value = 0.0;
      endfunction
      
	  // Set value
      task setValue(real input_val);
		value = input_val;
      endtask
		
	  // Add value
      task addValue(real input_val);
		value += input_val;
      endtask
		
	  // Get value
      function real getValue();
          return value;
      endfunction
	  
	  // Print value
	  task printValue();
		$display("Value: %f", value);
		$display("\n");
	  endtask
	  
endclass