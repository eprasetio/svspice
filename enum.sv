/*=================================================================================
****	iSPICE EE 537: enum.sv
**** -- File that define enums --
****
****	by Edwin Prasetio
****
**** Enums definition should be placed here
****
=================================================================================*/

typedef enum { RESISTOR, CAPACITOR, INDUCTOR, VS, CS, VCCS, VCVS, CCCS, CCVS, DIODE } element_t;
typedef enum { NONE, DC, OP, TRAN, NETLIST} action_t;
