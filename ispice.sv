/*********************************************************************************
****	iSPICE EE 537: ispice.sv
****	-- Main Program --
****
****	by Edwin Prasetio
****
**** SPICE Circuit Simulator Program using System Verilog
****
**********************************************************************************/
`timescale 1ns/1ps

`define NULL 0

program iSpice;
  // include class & enums
  ` include "enum.sv"
  ` include "elements.sv"
  ` include "stamp.sv"
  ` include "vstracker.sv"
  ` include "config.sv"
  
  //variables for linear time analysis
  real c_volt_temp;
  real c_curr_temp;
  real l_volt_temp;
  real l_curr_temp;
  Stamp vector_trans[int];

  Elements elmnts; 
  Elements elArray[int]; // storing elements
  string netlist_buff[int]; // storing buffered netlist
  
  int matrix_size; // for storing matrix & vectors dimension
  int max_node; // for storing the highest node number
  
  action_t action[int]; // for storing actions
  int action_index;
  
  VSTracker vs_tracker; // tracker for number of voltage source in ckt
  
  //matrices & vectors 
  Stamp matrix_a[int];
  Stamp matrix_lu[int];
  Stamp vector_y[int];
  Stamp vector_x[int];
  Stamp vector_b[int];
  Stamp stmp;
  
  //variables for diodes
  real vd;
  int converge;
  Stamp vector_xPrev[int];

  // Include all the necessary libraries
  ` include "tasksLib.sv"

/*********************************************************************************
**** Main program blocks 
**********************************************************************************/
 initial begin
  
  $display("\n================================================================================================");
  $display("##################################### Start running iSPICE #####################################");
  $display("================================================================================================\n\n");
  
  // initialize variable
  matrix_size = 0;
  max_node = 0;
  vs_tracker = new();
  action_index = 0;
  
  //time_max = 0;
  //time_step = 0;
  c_volt_temp = 0;
  c_curr_temp = 0;
  l_volt_temp = 0;
  l_curr_temp = 0;
  
  // initialize diode variable parameter to initial value
  vd = 0.0; 
  converge = 1;
  
  netlistParser(netlist_path); // start parsing netlist
  initMatrixAndVectors(); // initialize matrices and vectors
  executeAction(); // execute action based on the netlist
  
  $display("\n================================================================================================");
  $display("##################################### iSpice is Terminated #####################################");
  $display("================================================================================================\n\n");
  
  $display("Time: %d",$time); // display end clock

  #1 $finish;

 end
endprogram

 
